import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(Vuex)
Vue.use(VueAxios, axios)

const baseUri = 'https://reqres.in/api/users' // hardcode

export default new Vuex.Store({
  state: {
    users: [],
    total: 0
  },
  mutations: {
    getUsers(state, payload) {
      state.users = [...payload.data.data];
      Vue.set(state, 'page', payload.data.page);
      Vue.set(state, 'per_page', payload.data.per_page);
      Vue.set(state, 'total', payload.data.total);
      Vue.set(state, 'total_pages', payload.data.total_pages);
    },

    getUser(state, payload) {
      Vue.set(state, 'user', payload.data.data)
    },
  },
  actions: {
    async getUsers({commit}, [params]) {
      let uri = `${baseUri}/?per_page=${params.perPage}&page=${params.page}`

      const users = await Vue.axios.get(uri);
      commit('getUsers', users)
    },

    async getUser({commit}, id) {
      let uri = `${baseUri}/?id=${id}`

      const user = await Vue.axios.get(uri);
      commit('getUser', user)
    },
  },
  modules: {
  }
})

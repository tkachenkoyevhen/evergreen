import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('../views/Home.vue'),
    meta: {
      breadcrumbs: [
        {text: 'home', disabled: false, href: '/'}
      ]
    }
  },
  {
    path: '/users',
    name: 'Users',
    component: () => import('../views/Users.vue'),
    meta: {
      breadcrumbs: [
        {text: 'users', disabled: false, href: '/users'}
      ]
    }
  },
  {
    path: '/users/:id',
    name: 'User Detail',
    component: () => import('../views/UserDetail.vue'),
    meta: {
      breadcrumbs: [
        {text: 'users', disabled: false, href: '/users'},
        {text: 'user detail', disabled: false, href: '/users/:id'}
      ]
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
